package main

import (
	"parva.sprouts.api/src/services"

	"github.com/gin-gonic/gin"
)

func main() {

	router := gin.Default()

	router.Use(gin.Logger())

	v1 := router.Group("/api/v1/users")
	{
		v1.POST("/", services.CreateUser)
		v1.POST("/check", services.CheckUser)
		v1.GET("/", services.GetAllUsers)
		v1.GET("/:uuid", services.GetUser)
		v1.PUT("/:uuid", services.UpdateUser)
		v1.DELETE("/:uuid", services.DeleteUser)
	}
	router.Run()
}
