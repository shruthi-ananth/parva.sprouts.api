package repository

import (
	"fmt"

	"parva.sprouts.api/src/models"

	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

func SaveUser(firstName string, middleName string, lastName string, emailAddress string, password []byte) uuid.UUID {

	bytePassword := []byte(password)
	// Hashing the password with the default cost of 10
	hashedPassword, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	uuid := uuid.NewV4()
	user := models.User{Uuid: uuid, FirstName: firstName, MiddleName: middleName, LastName: lastName, EmailAddress: emailAddress, Password: hashedPassword}
	db, _ := Database()
	defer db.Close()

	if err != nil {
		panic(err)
	}

	db.Save(&user)
	return user.Uuid
}

func FindAllUsers() []models.User {
	var users []models.User
	db, _ := Database()
	defer db.Close()
	db.Find(&users)
	return users
}

func FindUserByUuid(uuid uuid.UUID) models.User {
	var user models.User
	db, _ := Database()
	defer db.Close()
	db.First(&user, uuid)
	return user
}

func UpdateUser(uuid uuid.UUID, firstName string, middleName string, lastName string) models.User {
	var user models.User
	db, _ := Database()
	defer db.Close()
	db.First(&user, uuid)
	db.Model(&user).Updates(models.User{FirstName: firstName, MiddleName: middleName, LastName: lastName})

	return user
}

func DeleteUser(uuid uuid.UUID) models.User {
	var user models.User
	db, _ := Database()
	defer db.Close()
	db.First(&user, uuid)
	db.Delete(&user)

	return user
}

func FindUserByEmailPassword(email string, password []byte) models.User {
	var user models.User
	db, _ := Database()
	defer db.Close()
	if err := db.Where("email_address = ?", email).First(&user).Error; err != nil {
		fmt.Println(err)
	}
	// Comparing the password with the hash
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), password)
	if err != nil {
		panic(err)
	}

	fmt.Println(user)
	return user
}
