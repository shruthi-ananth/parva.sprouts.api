package repository

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type DB struct {
	*gorm.DB
}

func Database() (*DB, error) {

	//open a db connection
	db, err := gorm.Open("mysql", "root:testing@tcp(mysql:3306)/parva?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}

	//Migrate the schema
	//db, _ := Database()
	//defer db.Close()
	//db.AutoMigrate(&User{})

	return &DB{db}, nil
}
