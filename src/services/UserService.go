package services

import (
	"net/http"

	"parva.sprouts.api/src/models"
	"parva.sprouts.api/src/objects"
	"parva.sprouts.api/src/repository"

	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
)

func CreateUser(c *gin.Context) {
	_user := models.User{}
	err := c.BindJSON(&_user)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": "Bad Request"})
		return
	}
	if err == nil {
		uuid := repository.SaveUser(_user.FirstName, _user.MiddleName, _user.LastName, _user.EmailAddress, _user.Password)
		c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "User created", "userUuid": uuid})
	}
}

func GetAllUsers(c *gin.Context) {
	var _users []objects.TransformedUser

	users := repository.FindAllUsers()
	if len(users) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user found"})
		return
	}

	//transforms the users for building a good response
	for _, item := range users {
		_users = append(_users, objects.TransformedUser{Uuid: item.Uuid, FirstName: item.FirstName, MiddleName: item.MiddleName,
			LastName: item.LastName, EmailAddress: item.EmailAddress})
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "result": _users})
}

func GetUser(c *gin.Context) {

	uuid, err := uuid.FromString(c.Param("uuid"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": "Bad Request"})
		return
	}

	user := repository.FindUserByUuid(uuid)
	if user.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user found"})
		return
	}

	_user := objects.TransformedUser{Uuid: user.Uuid, FirstName: user.FirstName, MiddleName: user.MiddleName,
		LastName: user.LastName, EmailAddress: user.EmailAddress}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "result": _user})
}

func UpdateUser(c *gin.Context) {
	uuid, err := uuid.FromString(c.Param("uuid"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": "Bad Request"})
		return
	}

	user := repository.UpdateUser(uuid, c.PostForm("firstName"), c.PostForm("middleName"), c.PostForm("lastName"))

	if user.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "User updated"})
}

func DeleteUser(c *gin.Context) {
	uuid, err := uuid.FromString(c.Param("uuid"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": "Bad Request"})
		return
	}

	user := repository.DeleteUser(uuid)

	if user.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "User deleted"})
}

func CheckUser(c *gin.Context) {

	_user := models.User{}
	err := c.BindJSON(&_user)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": "Bad Request"})
		return
	}
	if err == nil {
		user := repository.FindUserByEmailPassword(_user.EmailAddress, _user.Password)
		if user.ID == 0 {
			c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": "Incorrect login credentials"})
			return
		}

		_result := objects.TransformedUser{Uuid: user.Uuid, FirstName: user.FirstName, MiddleName: user.MiddleName,
			LastName: user.LastName, EmailAddress: user.EmailAddress}
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "result": _result})
	}
}
