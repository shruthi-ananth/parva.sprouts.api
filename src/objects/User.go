package objects

import "github.com/satori/go.uuid"

type TransformedUser struct {
	Uuid	uuid.UUID `json:"uuid"`
	FirstName     string `json:"firstName"`
	MiddleName    string `json:"middleName"`
	LastName      string `json:"lastName"`
	EmailAddress  string `json:"emailAddress"`
}
