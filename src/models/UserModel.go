package models

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

type User struct {
	gorm.Model
	Uuid	uuid.UUID `json:"uuid"`
	FirstName     string `json:"firstName"`
	MiddleName     string `json:"middleName"`
	LastName      string `json:"lastName"`
	EmailAddress string `json:"emailAddress" binding:"required"`
	Password []byte `json:"password" binding:"required"`
}
